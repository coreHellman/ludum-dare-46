local Make_object = require "make_object"
local Menu = require "menu"

close_shop = false

local Level_logic = {
  current_level = 1,
  levels = {},
  game_over = false,
  next_level = false,
  enemies = {},
  projectiles = nil,
  paused = true,
  world = nil
}

function Level_logic:load(world)
  local width = love.graphics.getWidth()
  local height = love.graphics.getHeight()

  for x = 1, self.current_level * 5 do
    random_table_X = {love.math.random(-(width/2), -(width/2)-20),
                      love.math.random(width/2, (width/2)+20),
                      love.math.random(-(width/2), width/2)}
    random_table_Y = {love.math.random(height/2, (height/2)+20),
                      love.math.random(-(height/2), -(height/2)-20)}

    x_choice = love.math.random(2)

    x = random_table_X[x_choice]

    if x_choice == 3 then
      y = random_table_Y[2]
    else
      y = love.math.random(height)
    end

    table.insert(self.enemies, Make_object:enemy(world, x, y, x))
  end

  -- Load Enemies
  for enemy_index, enemy in ipairs(self.enemies) do
    enemy:load(dt)
  end
end

function Level_logic:update(dt, world, player)

  if self.projectiles ~= nil then
    self.projectiles:update(dt, world)
  end


  if self.enemies[1] == nil then
    self.current_level = self.current_level + 1
    self:load(world)
  end

  if self.game_over == true then
    self.paused = true
  end

  -- Update Enemies
  for enemy_index, enemy in ipairs(self.enemies) do
    enemy:update(dt, player)
  end
end

function Level_logic:draw()


  if self.projectiles ~= nil then
    self.projectiles:draw(dt, world)
  end

  -- Draw Enemies
  for enemy_index, enemy in ipairs(self.enemies) do
    enemy:draw()
  end

  if self.game_over == true then
    love.graphics.setColor(0, 0, 0, .8)
    love.graphics.rectangle("fill", -love.graphics.getWidth()/2, -love.graphics.getHeight()/2, 1280, 720)
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.print("GAME OVER", - 80, -100)
  end

end

function Level_logic:get_enemies()
  return self.enemies
end

function Level_logic:remove_enemy(enemy)
  table.remove(self.enemies, enemy)
end

function Level_logic:get_world()
  return world
end

function Level_logic:open_shop_logic()

  if Level_logic.current_level % 2 == 0 and close_shop == false then
    Menu.is_showing = true
    self.paused = true
  end

  if close_shop == true then
    self.paused = false
  end

  if Level_logic.current_level % 2 ~= 0 then
    close_shop = false
  end

end

function love.keyreleased(key, scancode)
  if key == 'escape' then
    close_shop = true
    Menu.is_showing = false
  end
end


return Level_logic
