local Player = require "objects.player"
local Letter_t = require "objects.letter_t"
local Sword = require "objects.sword"
local Enemy = require "objects.enemy"
local Shield = require "objects.shield"
local Bow = require "objects.bow"
local Arrow = require "objects.arrow"
local Spear = require "objects.spear"

local Make_object = {}

function Make_object:player(world)
  local x = 0
  local y = 0
  local sprite_sheet = love.graphics.newImage('Sprites/I_Animation.png')
  local quad_table = {}
  for x = 1, 7 do
    table.insert(quad_table, love.graphics.newQuad((x - 1) * 32, 0, 32, 32, sprite_sheet:getDimensions()))
  end

  return Player.new(world, x, y, sprite_sheet, quad_table)
end

function Make_object:letter_t(world)
  local x = 0
  local y = 100
  local sprite_sheet = love.graphics.newImage('Sprites/t_Animation.png')
  local quad_table = {}
  for x = 1, 7 do
    table.insert(quad_table, love.graphics.newQuad((x - 1) * 32, 0, 32, 32, sprite_sheet:getDimensions()))
  end

  return Letter_t.new(world, x, y, sprite_sheet, quad_table)
end

function Make_object:enemy(world, x, y, enemy_number)
  local x = x
  local y = y
  local sprite_sheet = love.graphics.newImage('Sprites/Alphebet.png')
  local quad_table = {}
  for row = 1, 24 do
    quad_table[row] = {}
    for column = 1, 7 do
      quad_table[row][column] = love.graphics.newQuad((column - 1) * 34, (row - 1) * 34, 34, 34, sprite_sheet:getDimensions())
    end
  end

  return Enemy.new(world, x, y, sprite_sheet, quad_table[love.math.random(24)], enemy_number)
end

function Make_object:sword(world, parent)
  local sprite_sheet = love.graphics.newImage('Sprites/Weapons.png')
  local quad = love.graphics.newQuad(0, 34, 34, 34, sprite_sheet:getDimensions())

  return Sword.new(world, parent, sprite_sheet, quad)
end

function Make_object:shield(world, parent)
  local sprite_sheet = love.graphics.newImage('Sprites/Weapons.png')
  local quad = love.graphics.newQuad(34, 34, 34, 34, sprite_sheet:getDimensions())

  return Shield.new(world, parent, sprite_sheet, quad)
end

function Make_object:bow(world, parent)
  local sprite_sheet = love.graphics.newImage('Sprites/Weapons.png')
  local quad_table = {}

  for x = 1, 5 do
    table.insert(quad_table, love.graphics.newQuad((x - 1) * 34, 0, 34, 34, sprite_sheet:getDimensions()))
  end

  return Bow.new(world, parent, sprite_sheet, quad_table)
end

function Make_object:arrow(world, parent)
  local sprite_sheet = love.graphics.newImage('Sprites/Weapons.png')
  local quad = love.graphics.newQuad(102, 34, 34, 34, sprite_sheet:getDimensions())

  return Arrow.new(world, parent, sprite_sheet, quad)
end

function Make_object:spear(world, parent)
  local sprite_sheet = love.graphics.newImage('Sprites/Weapons.png')
  local quad = love.graphics.newQuad(64, 34, 34, 34, sprite_sheet:getDimensions())

  return Spear.new(world, parent, sprite_sheet, quad)
end


return Make_object
