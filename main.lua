local Make_object = require "make_object"
local Level_logic = require "level_logic"
local Menu = require "menu"

io.stdout:setvbuf("no")

can_pause = true
can_pause_timer = 10

game_start = false

function love.load()
  world = love.physics.newWorld(0, 0, true)

  world:setCallbacks(beginContact, endContact, preSolve, postSolve)



  player = Make_object:player(world)
  letter_t = Make_object:letter_t(world)

  player:add_to_inventory(Make_object:sword(world, player))

  player:load()
  letter_t:load()

  -- Load the level_logic
  Level_logic:load(world)

  Menu:load()

  load_ui()


end

function love.update(dt)

  if love.keyboard.isDown('p') and can_pause == true then
    if Level_logic.paused == false then
      Level_logic.paused = true
    else
      Level_logic.paused = false
    end

    can_pause = false
  end

  if can_pause_timer < 0 then
    can_pause = true
  else
    can_pause_timer = can_pause_timer - (1 * dt)
  end

  Level_logic:open_shop_logic()

  Menu:update(dt, player, world)

  if game_start == false then

    -- Main menu code here -------
    if love.mouse.isDown(1) then
      mos_x = love.mouse.getX()
      mos_y = love.mouse.getY()
      button = {x = ((love.graphics.getWidth() / 2) - 75),
                y = ((love.graphics.getHeight() / 2) - 75),
                w = 150,
                h = 130
                }
      if check_mouse_click(mos_x, mos_y, button) then
        game_start = true
        Level_logic.paused = false
      end
    end
  end

  -- Ask for pause
  if Level_logic.paused == false and game_start == true then

  -- Updates the physics engine.
  world:update(dt)

  player:update(dt)
  letter_t:update(dt, player, world)

  -- Update the level_logic
  Level_logic:update(dt, world, player)

end

end

function love.draw()

  -- Ask for pause
  --if Level_logic.paused == false then

    love.graphics.setColor(.94, .72, .46, 1)
    love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(),
                                          love.graphics.getHeight())
    love.graphics.setColor(0, 0, 0, 1)

    -- Print the players gold
    local myFont = love.graphics.newFont( "Arial.ttf", 32 )
    myFont:setFilter( "nearest", "nearest" )
    love.graphics.setFont(myFont)
    love.graphics.printf("Gold: " .. player.gold, 10, 10, 200, "center", 0, 1, 1)


    love.graphics.push()
    love.graphics.translate(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2)
    --love.graphics.scale(2, 2)

    -- Draw the level_logic
    Level_logic:draw(dt)

    love.graphics.setColor(1, 1, 1, 1)

    player:draw()
    letter_t:draw()

    Menu:draw()
    draw_ui()

    love.graphics.pop()

  --else

    -- Do this while paused

  --end

  if game_start == false then

    -- Main Menu code here-----
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())

    love.graphics.setColor(0, 0, 0, 1)
    love.graphics.rectangle("line", (love.graphics.getWidth() / 2) - 75, (love.graphics.getHeight() / 2) - 32, 150, 75)
    local myFont = love.graphics.newFont( "Arial.ttf", 32 )
    myFont:setFilter( "nearest", "nearest" )
    love.graphics.setFont(myFont)
    love.graphics.printf("PLAY", (love.graphics.getWidth() / 2) - 38, (love.graphics.getHeight() / 2) - 10, 80, "center", 0, 1, 1)
    local titleFont = love.graphics.newFont( "Arial.ttf", 64 )
    titleFont:setFilter( "nearest", "nearest" )
    love.graphics.setFont(titleFont)
    love.graphics.printf("Keep IT Alive", (love.graphics.getWidth() / 2) - 150, (love.graphics.getHeight() * .1) - 10, 300, "center", 0, 1, 1)

    love.graphics.setColor(0,0,0,0)
    love.graphics.rectangle("fill", (love.graphics.getWidth() / 2) - 75, (love.graphics.getHeight() / 2) - 32, 150, 75)
    love.graphics.setColor(0,0,0,1)
  end

end

----------------------------------------------------------physics calllback functions---------------------

function beginContact(a, b, coll)
  local enemies = Level_logic:get_enemies()

    if b:getUserData() == 'player' then
      player.health = player.health - .5
    elseif b:getUserData() == 'letter_t' then
      player.health = player.health - .5
    elseif a:getUserData() == 'player' then
      player.health = player.health - .5
    elseif a:getUserData() == 'letter_t' then
      player.health = player.health - .5
    else
      for enemy_index, enemy in ipairs(enemies) do
        if b:getUserData() == enemy.enemy_number and a:getUserData() == 'sword' then
          enemy.fixture:destroy()
          Level_logic:remove_enemy(enemy_index)
          player.gold = player.gold + 1
        end
      end

      for enemy_index, enemy in ipairs(enemies) do
        if a:getUserData() == enemy.enemy_number and b:getUserData() == 'sword' then
          enemy.fixture:destroy()
          Level_logic:remove_enemy(enemy_index)
          player.gold = player.gold + 1
        end
      end

      for enemy_index, enemy in ipairs(enemies) do
        if b:getUserData() == enemy.enemy_number and a:getUserData() == 'spear' then
          enemy.fixture:destroy()
          Level_logic:remove_enemy(enemy_index)
          player.gold = player.gold + 1
        end
      end

      for enemy_index, enemy in ipairs(enemies) do
        if a:getUserData() == enemy.enemy_number and b:getUserData() == 'spear' then
          enemy.fixture:destroy()
          Level_logic:remove_enemy(enemy_index)
          player.gold = player.gold + 1
        end
      end

      for enemy_index, enemy in ipairs(enemies) do
        if b:getUserData() == enemy.enemy_number and a:getUserData() == 'arrow' then
          enemy.fixture:destroy()
          Level_logic:remove_enemy(enemy_index)
          player.gold = player.gold + 1
        end
      end

      for enemy_index, enemy in ipairs(enemies) do
        if a:getUserData() == enemy.enemy_number and b:getUserData() == 'arrow' then
          enemy.fixture:destroy()
          Level_logic:remove_enemy(enemy_index)

          if Level_logic.projectiles ~= nil then
            if Level_logic.projectiles.fixture:isDestroyed() == false then
              Level_logic.projectiles.fixture:destroy()
            end
          end

          Level_logic.projectiles = nil

          player.gold = player.gold + 1
        end
      end
    end

end

function load_ui()
  numbers_sprite_sheet = love.graphics.newImage('Sprites/Numbers.png')
  heart_sprite = love.graphics.newQuad(32, 96, 32, 32, numbers_sprite_sheet:getDimensions())
  half_heart_sprite = love.graphics.newQuad(32, 96, 16, 32, numbers_sprite_sheet:getDimensions())
  gold_sprite = love.graphics.newImage('Sprites/gold.png')
  numbers = {}
  for x = 1, 4 do
    table.insert(numbers, love.graphics.newQuad((x - 1) * 32, 0, 32, 32, numbers_sprite_sheet:getDimensions()))
  end
  for x = 1, 4 do
    table.insert(numbers, love.graphics.newQuad((x - 1) * 32, 32, 32, 32, numbers_sprite_sheet:getDimensions()))
  end
  for x = 1, 2 do
    table.insert(numbers, love.graphics.newQuad((x - 1) * 32, 64, 32, 32, numbers_sprite_sheet:getDimensions()))
  end
end

function draw_ui()
  love.graphics.push()
  love.graphics.translate(425, -350)
  --Drawing hearts
  for x = 1, math.ceil(player.health) do
    if x == math.ceil(player.health) and player.health % 1 == 0.5 then
      love.graphics.draw(numbers_sprite_sheet, half_heart_sprite, x * 34, 0)
    else
      love.graphics.draw(numbers_sprite_sheet, heart_sprite, x * 34, 0)
    end
  end
  --Drawing gold amount
  --love.graphics.draw(gold_sprite, 0, 40)



  love.graphics.pop()
end

function endContact(a, b, coll)

end

function preSolve(a, b, coll)

end

function postSolve(a, b, coll, normalimpulse, tangentimpulse)

end
