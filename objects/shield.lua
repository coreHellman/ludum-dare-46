return{
  new = function(world, parent, sprite_sheet, quad)
    local shield = {
      parent = parent,
      x = parent.body:getX(),
      y = parent.body:getY(),
      body = love.physics.newBody(world, x, y, 'static'),
      shape = love.physics.newRectangleShape(10, 14),
      fixture = nil,
      sprite_sheet = sprite_sheet,
      name = 'shield',
      quad = quad
    }

    function shield:load()
      self.fixture = love.physics.newFixture(self.body, self.shape, 5)
      self.body:setLinearDamping(3)
      self.body:setFixedRotation(true)
      self.fixture:setUserData('shield')
      self.fixture:setCategory(2)
      self.fixture:setMask(1)

    end

    function shield:update(dt)

      if love.mouse.getX() - love.graphics.getWidth() / 2 < self.parent.body:getX() then
        self.body:setX(self.parent.body:getX() + 21)
        self.body:setY(self.parent.body:getY() + 20)
      elseif love.mouse.getX() - love.graphics.getWidth() / 2 > self.parent.body:getX() then
        self.body:setX(self.parent.body:getX() + 8)
        self.body:setY(self.parent.body:getY() + 20)
      end

    end

    function shield:draw()

      if love.mouse.getX() - love.graphics.getWidth() / 2 < self.parent.body:getX() then
        love.graphics.draw(self.sprite_sheet, self.quad, self.parent.body:getX() + 22, self.parent.body:getY() + 26, self.body:getAngle(), .5, .5 , 16, 28)
        --love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
      elseif love.mouse.getX() - love.graphics.getWidth() / 2 > self.parent.body:getX() then
        love.graphics.draw(self.sprite_sheet, self.quad, self.parent.body:getX() + 10, self.parent.body:getY() + 26, self.body:getAngle(), .5, .5 , 16, 28)
        --love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
      end

    end

    return shield
  end
}
