
return{
  new = function(world, x, y, sprite_sheet, quad_table)
    local letter_t = {
      inventory = {},
      body = love.physics.newBody(world, x, y, 'dynamic'),
      shape = love.physics.newRectangleShape(15, 18, 4, 25),
      fixture = nil,
      speed = 200,
      sprite_sheet = sprite_sheet,
      quad_table = quad_table,
      sprite_index = 1,
      sprite_index_count = 0
    }

    function letter_t:load()
      self.fixture = love.physics.newFixture(self.body, self.shape, 5)
      self.body:setLinearDamping(3)
      self.body:setFixedRotation(true)
      self.fixture:setUserData('letter_t')
      self.fixture:setCategory(1)
      self.fixture:setMask(1)
    end

    function letter_t:update(dt, player, world)
      local Level_logic = require "level_logic"

      -- Update the players current animation frame.
      if self.sprite_index_count <= 10 then
        self.sprite_index_count = self.sprite_index_count + (100 * dt)
      else
        self.sprite_index = self.sprite_index + 1
        self.sprite_index_count = 0
      end

      if self.sprite_index > #self.quad_table then
        self.sprite_index = 1
      end
      -- End animation frame handle

      if self.body:getX() < player.body:getX() + 20 then
        self.body:applyForce(self.speed, 0)
      elseif self.body:getX() > player.body:getX() + 20 then
        self.body:applyForce(-self.speed, 0)
      end

      if self.body:getY() < player.body:getY() then
        self.body:applyForce(0, self.speed)
      elseif self.body:getY() > player.body:getY() then
        self.body:applyForce(0, -self.speed)
      end

      -- updates letter_t equipment.
      for inventory_index, object in ipairs(self.inventory) do
        object:update(dt)
      end

    end

    function letter_t:draw()

      -- Draw the letter_t sprite
      love.graphics.draw(self.sprite_sheet, self.quad_table[self.sprite_index],
                         self.body:getX(), self.body:getY(), self.body:getAngle())

      -- Draw letter_t physics box
      --love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))

      -- Draws letter_t equipment.
      for inventory_index, object in ipairs(self.inventory) do
        object:draw()
      end
    end

    function letter_t:add_to_inventory(object)
      table.insert(self.inventory, object)
    end

    return letter_t
  end
}
