return{
  new = function(world, parent, sprite_sheet, quad_table)
    local bow = {
      parent = parent,
      world = world,
      x = parent.body:getX(),
      y = parent.body:getY(),
      sprite_sheet = sprite_sheet,
      can_fire = true,
      quad_table = quad_table,
      firing = false,
      closest_enemy = nil,
      firing_timer = 10,
      can_fire_timer = 30,
      sprite_index = 1,
      fired = false,
      name = 'bow',
      arrow = nil,
      rotation = 0
    }

    function bow:load()
      if self.arrow ~= nil then
        self.arrow:load()
      end
    end

    function bow:update(dt)

      self.closest_enemy = self:find_closest_enemy()

      -- Controls the animation for the bow
      if self.firing == false and self.can_fire == true then
        self.firing = true
        self.can_fire = false
      end

      if self.firing == true then
        if self.firing_timer <= 0 then
          self.sprite_index = self.sprite_index + 1
          self.firing_timer = 10
        else
          self.firing_timer = self.firing_timer - (50 * dt)
        end
        if self.sprite_index > 5 then
          self.sprite_index = 1
          self.firing = false
          self.fired = false
        end
      end

      -- Controls the arrow from the bow
      if self.sprite_index == 5 and self.fired == false then
        local Make_object = require "make_object"
        local Level_logic = require "level_logic"

        Level_logic.projectiles = Make_object:arrow(world, self)
        self.fired = true
      end

      -- Cotrols the rate of fire for the bow
      if self.can_fire == false then
        if self.can_fire_timer <= 0 then
          self.can_fire = true
          self.can_fire_timer = 30
        else
          self.can_fire_timer = self.can_fire_timer - (10 * dt)
        end
      end


      --Letter_t pointing the bow
      if self.closest_enemy ~= nil then
        local x1 = self.parent.body:getX() + 16
        local y1 = self.parent.body:getY() + 16
        local x2 = self.closest_enemy.body:getX() + 8
        local y2 = self.closest_enemy.body:getY() + 8
        self.rotation = calculate_angle(x1, y1, x2, y2) + math.pi
      end

    end

    function bow:draw()

      if self.rotation < 1.8 or self.rotation > 4.6 then
        love.graphics.draw(self.sprite_sheet, self.quad_table[self.sprite_index], self.parent.body:getX() + 20, self.parent.body:getY() + 16, self.rotation, 1, 1, 16, 16)
      else
        love.graphics.draw(self.sprite_sheet, self.quad_table[self.sprite_index], self.parent.body:getX() + 10, self.parent.body:getY() + 16, self.rotation, 1, 1, 16, 16)
      end

      if self.closest_enemy ~= nil then
        -- Draws line to closest enemy.
        --love.graphics.line(self.parent.body:getX() + 16, self.parent.body:getY() + 16, self.closest_enemy.body:getX() + 16, self.closest_enemy.body:getY() + 16)
      end
    end

    function bow:find_closest_enemy()
      local Level_logic = require "level_logic"
      local closest_enemy = nil

      local enemies = Level_logic:get_enemies()

      if #enemies ~= 0 then
        for enemy_index, enemy in ipairs(enemies) do
          local x1 = self.parent.body:getX() + 16
          local y1 = self.parent.body:getY() + 16
          local x2 = enemy.body:getX()
          local y2 = enemy.body:getY()

          enemy.distance = calculate_distance(x1, y1, x2, y2)

          if closest_enemy == nil then
            closest_enemy = enemy
          elseif enemy.distance < closest_enemy.distance then
            closest_enemy = enemy
          end

        end
      else
        return nil
      end

      return closest_enemy
    end

    function calculate_angle(x1, y1, x2, y2)
      return math.atan2(y1 - y2 , x1 - x2)
    end

    function calculate_distance(x1, y1, x2, y2)
      return math.sqrt(math.abs(math.pow(x2 - x1, 2)) + math.abs(math.pow(y2 - y1, 2)))
    end

    return bow
  end
}
