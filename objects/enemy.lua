
return{
  new = function(world, x, y, sprite_sheet, quad_table, enemy_number)
    local enemy = {
      inventory = {},
      body = love.physics.newBody(world, x, y, 'dynamic'),
      shape = love.physics.newRectangleShape(15, 20, 7, 22),
      fixture = nil,
      speed = love.math.random(100,200),
      distance = 0,
      attack = false,
      name = 'enemy',
      sprite_sheet = sprite_sheet,
      quad_table = quad_table,
      sprite_index = 1,
      sprite_index_count = 0,
      enemy_number = enemy_number
    }

    function enemy:load()
      self.fixture = love.physics.newFixture(self.body, self.shape, 5)
      self.body:setLinearDamping(3)
      self.body:setFixedRotation(true)
      self.fixture:setUserData(self.enemy_number)
      self.fixture:setCategory(3)
      --self.fixture:setMask()
      self.fixture:setRestitution(1)

    end

    function enemy:update(dt)
      -- Update the enemies current animation frame.
      if self.sprite_index_count <= 10 then
        self.sprite_index_count = self.sprite_index_count + (100 * dt)
      else
        self.sprite_index = self.sprite_index + 1
        self.sprite_index_count = 0
      end

      if self.sprite_index > #self.quad_table then
        self.sprite_index = 1
      end
      -- End animation frame handle

      if self.body:getX() < player.body:getX() then
        self.body:applyForce(self.speed, 0)
      elseif self.body:getX() > player.body:getX() then
        self.body:applyForce(-self.speed, 0)
      end

      if self.body:getY() < player.body:getY() then
        self.body:applyForce(0, self.speed)
      elseif self.body:getY() > player.body:getY() then
        self.body:applyForce(0, -self.speed)
      end

    end

    function enemy:draw()

      -- Draw the enemies sprite
      love.graphics.draw(self.sprite_sheet, self.quad_table[self.sprite_index],
                         self.body:getX(), self.body:getY(), self.body:getAngle())

      -- Draw enemies physics box
      --love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))

    end

    return enemy
  end
}
