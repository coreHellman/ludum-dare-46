
return{
  new = function(world, x, y, sprite_sheet, quad_table)
    local player = {
      inventory = {},
      body = love.physics.newBody(world, x, y, 'dynamic'),
      shape = love.physics.newRectangleShape(15, 20, 7, 22),
      fixture = nil,
      name = 'player',
      gold = 1000,
      health = 5,
      speed = 400,
      attack = false,
      sprite_sheet = sprite_sheet,
      quad_table = quad_table,
      sprite_index = 1,
      sprite_index_count = 0
    }

    function player:load()
      self.fixture = love.physics.newFixture(self.body, self.shape, 5)
      self.body:setLinearDamping(3)
      self.body:setFixedRotation(true)
      self.fixture:setUserData(self.name)
      self.fixture:setCategory(1)


      -- Load player equipment.
      for inventory_index, object in ipairs(self.inventory) do
        object:load()
      end
    end

    function player:update(dt)
      local Level_logic = require "level_logic"

      if self.health <= 0 then
        Level_logic.game_over = true
      end

      -- Update the players current animation frame.
      if self.sprite_index_count <= 10 then
        self.sprite_index_count = self.sprite_index_count + (100 * dt)
      else
        self.sprite_index = self.sprite_index + 1
        self.sprite_index_count = 0
      end

      if self.sprite_index > #self.quad_table then
        self.sprite_index = 1
      end
      -- End animation frame handle

      -- Update player equipment.
      for inventory_index, object in ipairs(self.inventory) do
        object:update(dt)
      end

      -- Player Control Handle
      self:controls()
    end

    function player:draw()

      -- Draw the players sprite
      love.graphics.draw(self.sprite_sheet, self.quad_table[self.sprite_index],
                         self.body:getX(), self.body:getY(), self.body:getAngle())

      -- Draw player physics box
      --love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))

      -- Draws player equipment.
      for inventory_index, object in ipairs(self.inventory) do
        object:draw()
      end
    end

    function player:add_to_inventory(object)

      if object.name ~= 'shield' then

        for inventory_index, something in ipairs(self.inventory) do
          table.remove(self.inventory, inventory_index)
        end
        table.insert(self.inventory, object)
        for inventory_index, something_else in ipairs(self.inventory) do
          something_else:load()
        end
      else
        table.insert(self.inventory, object)
        for inventory_index, something_else in ipairs(self.inventory) do
          something_else:load()
        end
      end
    end

    function player:clear_inventory()
      for x = 1, #self.inventory do
        table.remove(self.inventory, x)
      end
    end

    function player:controls()
      -- Control the up and down.
      if love.keyboard.isDown('w') then
        self.body:applyForce(0, -self.speed)
      elseif love.keyboard.isDown('s') then
        self.body:applyForce(0, self.speed)
      end
      -- Controls the left and right
      if love.keyboard.isDown('a') then
        self.body:applyForce(-self.speed, 0)
      elseif love.keyboard.isDown('d') then
        self.body:applyForce(self.speed, 0)
      end

      if love.mouse.isDown(1) then
        self.attack = true
      end

    end

    return player
  end
}
