return{
  new = function(world, parent, sprite_sheet, quad)
    local arrow = {
      parent = parent,
      x = 0,
      y = 0,
      body = love.physics.newBody(world, x, y, "dynamic"),
      shape = love.physics.newRectangleShape(2, 10),
      sprite_sheet = sprite_sheet,
      quad = quad,
      speed = 100,
      rotation = 0,
      loaded = false
    }

    function arrow:load()
    end

    function arrow:update(dt, world)
      self.rotation = self.parent.rotation
      self.body:setAngle(self.rotation)

      if self.loaded == false then
        self.x = parent.parent.body:getX() + 16
        self.y = parent.parent.body:getY() + 16
        self.body:setPosition(self.x, self.y)

        self.fixture = love.physics.newFixture(self.body, self.shape, 1)
        self.body:setFixedRotation(true)
        self.fixture:setUserData('arrow')
        self.fixture:setCategory(1)
        self.fixture:setMask(1)
        self.loaded = true
      end

      self.body:applyLinearImpulse(1*math.cos(self.rotation), 1*math.sin(self.rotation))

    end

    function arrow:draw()

      if self.fixture:isDestroyed() == false then

        if self.rotation < 1.8 or self.rotation > 4.6 then
          love.graphics.draw(self.sprite_sheet, self.quad, self.body:getX(), self.body:getY(), self.rotation, 1, 1, 16, 16)
        else
          love.graphics.draw(self.sprite_sheet, self.quad, self.body:getX(), self.body:getY(), self.rotation, 1, 1, 16, 16)
        end

        love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))

      end

    end

    return arrow
  end
}
