
return{
  new = function(world, parent, sprite_sheet, quad)
    local sword = {
      parent = parent,
      x = parent.body:getX(),
      y = parent.body:getY(),
      body = love.physics.newBody(world, x, y, 'static'),
      shape = love.physics.newRectangleShape(4, 40),
      fixture = nil,
      attack_index = math.pi,
      sprite_sheet = sprite_sheet,
      name = 'sword',
      quad = quad
    }

    function sword:load()
      self.fixture = love.physics.newFixture(self.body, self.shape, 5)
      self.body:setLinearDamping(3)
      self.body:setFixedRotation(true)
      self.fixture:setUserData('sword')
      self.fixture:setCategory(1)
      self.fixture:setMask(1, 3)

    end

    function sword:update(dt)
      if self.parent.attack == true then
        self.fixture:setMask(1)
        if love.mouse.getX() - love.graphics.getWidth() / 2 < self.parent.body:getX() then
          self.body:setAngle(self.body:getAngle() + (-10 * dt))
        else
          self.body:setAngle(self.body:getAngle() + (10 * dt))
        end

        if math.abs(self.body:getAngle()) > self.attack_index then
          self.parent.attack = false
          self.body:setAngle(0)
        end
      else
        self.fixture:setMask(1, 3)
      end

      if love.mouse.getX() - love.graphics.getWidth() / 2 < self.parent.body:getX() then
        self.body:setX(self.parent.body:getX() + 9)
        self.body:setY(self.parent.body:getY() + 17)
      elseif love.mouse.getX() - love.graphics.getWidth() / 2 > self.parent.body:getX() then
        self.body:setX(self.parent.body:getX() + 19)
        self.body:setY(self.parent.body:getY() + 17)
      end

    end

    function sword:draw()

      if love.mouse.getX() - love.graphics.getWidth() / 2 < self.parent.body:getX() then
        love.graphics.draw(self.sprite_sheet, self.quad, self.parent.body:getX() + 10, self.parent.body:getY() + 22, self.body:getAngle(), 1, 1 , 16, 28)
        --love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
      elseif love.mouse.getX() - love.graphics.getWidth() / 2 > self.parent.body:getX() then
        love.graphics.draw(self.sprite_sheet, self.quad, self.parent.body:getX() + 20, self.parent.body:getY() + 22, self.body:getAngle(), 1, 1 , 16, 28)
        --love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
      end

    end

    return sword
  end
}
