local Make_object = require "make_object"

local Menu = {
  background = nil,
  pos_x = 0,
  pos_y = 0,
  width = 0,
  height = 0,
  sold_items = {},
  prices = {20, 250, 500, 1000},
  is_showing = false,
  is_checking = false,
  is_pressed = false,
  timer = 5,
  buttons = {
    heart =        {x = 760, y = 52,  w = 150, h = 90, cost = 20},
    shield =       {x = 304, y = 176, w = 670, h = 90, cost = 250},
    spear =        {x = 304, y = 300, w = 670, h = 90, cost = 500},
    bow =          {x = 304, y = 423, w = 670, h = 90, cost = 1000},
    pitfall_trap = {x = 304, y = 545, w = 170, h = 90, cost = 150},
    spike_trap =   {x = 560, y = 545, w = 170, h = 90, cost = 75},
    blockade =     {x = 805, y = 545, w = 170, h = 90, cost = 50}
  }
}

function Menu:load()
  self.background = love.graphics.newImage("Sprites/shop.png")
  self.width = self.background:getWidth() / 2
  self.height = self.background:getHeight() / 2
end

function Menu:update(dt, player, letter_t)
  if love.keyboard.isDown('m') then
    self.is_showing = true
  end
  if self.is_showing == true then
    if love.mouse.isDown(1) and self.is_checking == false then
      self.is_checking = true
      self:buy_item()
    end
  end

  if self.timer <= 0 then
    self.is_checking = false
    self.timer = 5
  else
    self.timer = self.timer - (10 * dt)
  end

end

function Menu:draw()
  if self.is_showing then
    love.graphics.draw(self.background, 0, 0, 0, 1.5, 1.5, self.width, self.height)
    --love.graphics.print("Mouse: " .. love.mouse.getX() .. ", " .. love.mouse.getY(), -200, -200)
  end

end

function Menu:buy_item()
  mos_x = love.mouse.getX()
  mos_y = love.mouse.getY()
  if check_mouse_click(mos_x, mos_y, self.buttons.heart) == true then
    if player.gold > self.buttons.heart.cost then
      if player.health == 4.5 then
        player.health = 5
        player.gold = player.gold - self.buttons.heart.cost
      elseif player.health < 5 then
        player.health = player.health + 1
        player.gold = player.gold - self.buttons.heart.cost
      end
    end
  elseif check_mouse_click(mos_x, mos_y, self.buttons.shield) == true then
    if player.gold > self.buttons.shield.cost then
      --add the shield to the player
      player:add_to_inventory(Make_object:shield(world, player))
      player.gold = player.gold - self.buttons.shield.cost
      self.buttons.shield.cost = 100000
      --block off shield
    end
  elseif check_mouse_click(mos_x, mos_y, self.buttons.spear) == true then
    if player.gold > self.buttons.spear.cost then
      --add the spear to player
      player:add_to_inventory(Make_object:spear(world, player))
      player.gold = player.gold - self.buttons.spear.cost
      self.buttons.spear.cost = 100000
      --block off spear
    end
  elseif check_mouse_click(mos_x, mos_y, self.buttons.bow) == true then
    if player.gold > self.buttons.bow.cost then
      --Add the bow to letter t
      letter_t:add_to_inventory(Make_object:bow(world, letter_t))
      player.gold = player.gold - self.buttons.bow.cost
      self.buttons.bow.cost = 100000
      --block off bow
    end
  end

end

function check_mouse_click(mouse_x, mouse_y, button)
  if mouse_x > button.x and mouse_x < button.x + button.w and mouse_y > button.y and mouse_y < button.y + button.h then
    return true
  else
    return false
  end
  return false
end

--player:add_to_inventory()

return Menu
